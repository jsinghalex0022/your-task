#include <stdio.h>

int main()
{
    int n;
    int count=0;
    printf("Enter the nth value:\n");
    scanf("%d",&n);

    int arr[n];
    printf("Enter the numbers:\n");
    for (int l = 0; l < n; ++l) {
        scanf("%d",&arr[l]);
    }

    for (int i = 0; i < n; ++i) {
        count=1;
        for (int j = i+1; j < n; ++j) {
            if(arr[i]==arr[j]){
                count=count+1;
                arr[j]=0;
            }
        }
        if(arr[i]!=0){
            printf("%d is %d\n times:",arr[i],count);
        }
    }

    return 0;
}